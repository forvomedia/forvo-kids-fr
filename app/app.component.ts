import { Component, OnInit } from '@angular/core';
import { Page } from 'tns-core-modules/ui/page';
import { DeviceService } from './core/service/device.service';
import { DbService } from './core/service/device/db.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html'
})
export class AppComponent implements OnInit {
  constructor(
    page: Page,
    private deviceService: DeviceService,
    private dbService: DbService
  ) {
    page.actionBarHidden = true;
    const deviceIsIOS = this.deviceService.deviceIsIOS();

    if (deviceIsIOS) {
      if (this.deviceService.deviceIsIPhoneX()) {
        page.className = 'iphonex';
      } else {
        page.className = 'iphone';
      }
    }
  }

  ngOnInit() {
    this.dbService.initDB();
  }
}
