import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { forkJoin } from 'rxjs/observable/forkJoin';
import { map } from 'rxjs/operators';
import { Page } from 'tns-core-modules/ui/page';
import { Category } from '~/core/model/category';
import { DeviceService } from '~/core/service/device.service';
import { find, mapCollection, sortByNumber, sumUpValues } from '~/core/utils/collection.util';
import { ParentsService } from '~/parents/parents.service';
import { Stat } from '~/parents/stats/stat';
import { getLastMillisOfMonth, getFirstMillisOfMonth } from '~/core/utils/date.util';
import { TranslationService } from '~/core/service/translation.service';
import { TRANSLATION_LESS_PLAYED_CATEGORIES } from '~/core/config/constants';
import { capitalizeFirstLetter } from '~/core/utils/string.util';

const createCategoryStat = (totalVisits: number, stats: any[]) => (category: Category) => {
  const categoryStat = find(stat => stat['categoryId'] === category.id, stats);
  const categoryVisits = (categoryStat) ? categoryStat['visits'] : 0;
  const value = (totalVisits) ? categoryVisits / totalVisits : 0;

  const _categoryName: string = capitalizeFirstLetter(category.name);

  return new Stat(_categoryName, value);
};

const buildCategoryStats = (categories: Category[], stats: any[]) => {
  const visits = mapCollection(stat => stat['visits'], stats);
  const totalVisits = sumUpValues(visits);
  const categoryStats = mapCollection(createCategoryStat(totalVisits, stats), categories);

  return sortByNumber('value', categoryStats);
};

@Component({
  selector: 'app-categories-stats',
  moduleId: module.id,
  templateUrl: './categories-stats.component.html',
  styleUrls: ['./categories-stats.component.css']
})
export class CategoriesStatsComponent implements OnInit {
  stats$: Observable<Stat[]>;
  translations: string[];
  translation_less_played_categories: string;

  constructor(
    private parentsService: ParentsService,
    private deviceService: DeviceService,
    private translationService: TranslationService,
    page: Page
  ) {
    page.actionBarHidden = true;

    if (this.deviceService.deviceIsIOS()) {
      if (this.deviceService.deviceIsIPhoneX()) {
        page.className = 'iphonex';
      } else {
        page.className = 'iphone';
      }
    }

    const translationIds = [TRANSLATION_LESS_PLAYED_CATEGORIES];
    this.translation_less_played_categories = TRANSLATION_LESS_PLAYED_CATEGORIES;
    this.translations = this.translationService.getSectionTranslations(translationIds);
  }

  ngOnInit() {
    this.getTotalStats();
  }

  onFilterChanged(date) {
    if (date) {
      this.getFilteredStats(date);
    } else {
      this.getTotalStats();
    }
  }

  private getTotalStats() {
    this.stats$ = forkJoin(
      this.parentsService.getCategories(),
      this.parentsService.getVisitedCategories()
    ).pipe(
      this.handleResponse()
    );
  }

  private getFilteredStats(date: number) {
    const start = getFirstMillisOfMonth(date);
    const end = getLastMillisOfMonth(date);
    this.stats$ = forkJoin(
      this.parentsService.getCategories(),
      this.parentsService.getVisitedCategoriesBetweenDates(start, end)
    ).pipe(
      this.handleResponse()
    );
  }

  private handleResponse = () => map((data: any) => {
    const [categories, stats] = data;

    return buildCategoryStats(categories, stats);
  })

  getTranslation(id: string) {
    return (this.translations) ? this.translations[id] : null;
  }
}
