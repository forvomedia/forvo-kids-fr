import { NgModule } from '@angular/core';
import { Routes } from '@angular/router';
import { NativeScriptRouterModule } from 'nativescript-angular';
import { CategoriesStatsComponent } from '~/parents/stats/categories/categories-stats.component';
import { WordsStatsComponent } from '~/parents/stats/words/words-stats.component';
import { ParentsHomeComponent } from './home/parents-home.component';
import { InfoComponent } from './info/info.component';

const parentsRoutes: Routes = [
  { path: '', component: ParentsHomeComponent },
  { path: 'info', component: InfoComponent },
  { path: 'categories', component: CategoriesStatsComponent },
  { path: 'words', component: WordsStatsComponent}
];

@NgModule({
  imports: [
    NativeScriptRouterModule.forChild(parentsRoutes)
  ]
})
export class ParentsRoutingModule { }
