import * as moment from 'moment';
import {capitalizeFirstLetter} from '~/core/utils/string.util';

export const getCurrentMillis = () => moment().locale('fr').valueOf();

export const subtractMonthsToDate = (date: number, months: number) => {
    return moment(date).locale('fr').subtract(months, 'month').valueOf();
};

export const formatDate = (date: number, format: string) =>
capitalizeFirstLetter(moment(date).locale('fr').format(format));

export const getFirstMillisOfMonth = (date: number) => moment(date).locale('fr').startOf('month').valueOf();

export const getLastMillisOfMonth = (date: number) => moment(date).locale('fr').endOf('month').valueOf();
