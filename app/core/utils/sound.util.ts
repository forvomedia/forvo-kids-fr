import { TNSPlayer } from 'nativescript-audio';
import { checkFile, getAbsolutePath } from './file.util';

export const playFromFile = (audioFile: string, completeCallback = null) => {
  const player = new TNSPlayer();
  if (checkFile(audioFile)) {
    player.playFromFile({ audioFile: getAbsolutePath(audioFile), loop: false, completeCallback});
  }

  return player;
};

export const loadFromFile = (audioFile: string, completeCallback: () => void) => {
  const player = new TNSPlayer();
  if (checkFile(audioFile)) {
    player.playFromFile({ audioFile: getAbsolutePath(audioFile), loop: true, completeCallback });
  }

  return player;
};
