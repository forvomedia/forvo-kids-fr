import { isNullOrUndefined } from 'tns-core-modules/utils/types';

export const sum = (a: number, b: number) => a + b;

export const parseNumber = (value: string) => (isNullOrUndefined(value)) ? null : +value;

export const compare = (a: number, b: number) => a - b;
