import { AUDIO_GAME_ITEM_FOLDER, IMAGE_GAME_ITEM_FOLDER } from '../config/constants';

export class GameItem {
  public successful: boolean | 'none' = 'none';

  constructor(
    public id: number,
    public name: string,
    private imagePath: string,
    private audioPath: string,
    private folder: string
  ) { }

  get image() {
    return `${ IMAGE_GAME_ITEM_FOLDER }${ this.folder }/${ this.imagePath }`;
  }

  get audio() {
    return `${ AUDIO_GAME_ITEM_FOLDER }${ this.folder }/${ this.audioPath }`;
  }

  sendFeedback(success: boolean) {
    this.successful = success;
    if (!success) {
      setTimeout(() => this.successful = 'none', 1000);
    }
  }
}
