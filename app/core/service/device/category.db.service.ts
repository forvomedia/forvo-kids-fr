import { Injectable } from '@angular/core';
import { mapCollection } from '~/core/utils/collection.util';
import { TABLE_CATEGORY_VISITED } from './db.constants';
import { DbService } from './db.service';
import { formatDate } from '~/core/utils/date.util';

@Injectable()
export class CategoryDbService {

  constructor(private dbService: DbService) { }

  async insertCategoryVisit(category: number) {
    const db = await this.dbService.getDbConnection();
    try {
      /* tslint:disable:max-line-length */
      await db.execSQL(`INSERT INTO ${ TABLE_CATEGORY_VISITED } (category_id, date) VALUES (?, date('now'))`, [category]);
      /* tslint:enable:max-line-length */
    } catch (error) {
      console.error('Error inserting category visit', error);
    } finally {
      db.close();
    }
  }

  async getCategoryVisits() {
    const db = await this.dbService.getDbConnection();
    try {
      const res = await db.all(`SELECT category_id, count(1) as visits FROM ${ TABLE_CATEGORY_VISITED } GROUP BY category_id`, []);

      return mapCollection(row => ({ categoryId: row[0], visits: row[1] }), res);
    } catch (errors) {
      return [];
    } finally {
      db.close();
    }
  }

  async getCategoryVisitsBetweenDates(start: number, end: number) {
    const db = await this.dbService.getDbConnection();
    try {
      const startDate = formatDate(start, 'YYYY-MM-DD');
      const endDate = formatDate(end, 'YYYY-MM-DD');
      /* tslint:disable:max-line-length */
      const res = await db.all(`SELECT category_id, count(1) as visits FROM ${ TABLE_CATEGORY_VISITED } WHERE date >= '${ startDate }' AND date <= '${ endDate }' GROUP BY category_id`);
      /* tslint:enable:max-line-length */

      return mapCollection(row => ({ categoryId: row[0], visits: row[1] }), res);
    } catch (errors) {
      return [];
    } finally {
      db.close();
    }
  }
}
