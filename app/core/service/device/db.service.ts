import { Injectable } from '@angular/core';
import { DB_NAME, TABLE_CATEGORY_VISITED, TABLE_WORD_FAILED } from './db.constants';

const Sqlite = require('nativescript-sqlite');

@Injectable()
export class DbService {

  getDbConnection() {
    return new Sqlite(DB_NAME);
  }

  async closeDbConnection() {
    const db = await this.getDbConnection();
    db.close();
  }

  async initDB() {
    const db = await this.getDbConnection();
    try {
      /* tslint:disable:max-line-length */
      await db.execSQL(`CREATE TABLE IF NOT EXISTS ${ TABLE_CATEGORY_VISITED } (id INTEGER PRIMARY KEY AUTOINCREMENT, category_id INTEGER, date DATE)`);
      await db.execSQL(`CREATE TABLE IF NOT EXISTS ${ TABLE_WORD_FAILED } (id INTEGER PRIMARY KEY AUTOINCREMENT, category_id INTEGER, word_id INTEGER, failure NUMERIC NOT NULL DEFAULT 0, date DATE)`);
      /* tslint:enable:max-line-length */
    } catch (error) {
      console.error('Error on creating DB', error);
    } finally {
      db.close();
    }
  }
}
