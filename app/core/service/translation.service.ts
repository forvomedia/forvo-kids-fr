import { of } from 'rxjs/observable/of';
import { DATA_FOLDER } from '../config/constants';
import { Translation } from '../model/translation';
import { readJson } from '../utils/file.util';

export class TranslationService {
  private translations: Translation[];

  constructor() {
    const translationFile = `${ DATA_FOLDER }translations.json`;
    this.translations = readJson(translationFile).map(
        (json) => new Translation( json.id, json.value )
      );
  }

  private _getTranslationById(translationId: string) {
    return of(this.translations.find(translation => translation.id === translationId));
  }

  private getTranslationById(translationId: string) {
    return this._getTranslationById(translationId).toPromise();
  }

  getSectionTranslations(translationIds: string[]) {
    const translations = [];
    translationIds.forEach(id => {
      this.getTranslationById(id).then((translation) => {
        translations[id] = translation.value;
      });
    });
    return translations;
  }
}
