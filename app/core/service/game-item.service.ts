import { of } from 'rxjs/observable/of';
import { DATA_FOLDER } from '../config/constants';
import { Category } from '../model/category';
import { GameItem } from '../model/game-item';
import { readJson } from '../utils/file.util';

export class GameItemService {
  static getWordsByCategory(category: Category) {
    const gameItemFile = `${ DATA_FOLDER }game-item/${ category.items }`;
    return readJson(gameItemFile).map(
      (json) => new GameItem(+json.id, json.name, json.image, json.audio, category.folder)
    );
  }

  getItemsByCategory(category: Category) {
    return of(GameItemService.getWordsByCategory(category));
  }
}
