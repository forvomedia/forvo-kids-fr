import { Injectable } from '@angular/core';
import { exitEvent, on as applicationOn, resumeEvent, suspendEvent } from 'application';
import { TNSPlayer } from 'nativescript-audio';
import { loadFromFile, playFromFile } from '../utils/sound.util';
import { ERROR_AUDIO, HOME_AUDIO } from '../config/constants';

@Injectable()
export class PlayerService {
  private _player: TNSPlayer;
  private audio: string;
  private _errorPlayerOne: TNSPlayer;
  private _errorPlayerTwo: TNSPlayer;
  private _errorPlayerThree: TNSPlayer;

  constructor() {
    applicationOn(suspendEvent, () => {
      if (this.player) {
        this.player.dispose();
        this.player = null;
      }
    });
    applicationOn(resumeEvent, () => {
      if (this.player) {
        this.player.play();
      } else {
        if (this.audio && this.audio === HOME_AUDIO) {
          this.playOnLoop(this.audio, true);
        }
      }
    });
    applicationOn(exitEvent, () => {
      if (this.player) {
        this.player.dispose();
      }
    });
  }

  playOnLoop(audio: string, force: boolean = false) {
    if (force === true || audio !== this.audio) {
      const player = loadFromFile(audio, () => player.play());
      this.player = player;
      this.audio = audio;
    }

    return this.player;
  }

  play(audio: string, callback = null) {
    if (audio !== ERROR_AUDIO) {
      this.player = playFromFile(audio, callback);
      this.audio = audio;
    } else {
      if (!this._errorPlayerOne) {
        this._errorPlayerOne = playFromFile(audio, callback);
        this._errorPlayerTwo = playFromFile(audio, callback);
        this._errorPlayerThree = playFromFile(audio, callback);
      } else {
        if (!this._errorPlayerOne.isAudioPlaying()) {
          this._errorPlayerOne.play();
        } else {
          if (!this._errorPlayerTwo.isAudioPlaying()) {
            this._errorPlayerTwo.play();
          } else {
            this._errorPlayerThree.play();
          }
        }
      }
    }

    return this.player;
  }

  replay() {
    if (this.player) {
      this.player.play();
    } else {
      if (this.audio) {
        this.play(this.audio);
      }
    }
  }

  stop() {
    if (this.player) {
      this.player.dispose();
      this.audio = null;
      this.player = null;
    }
  }

  get player() {
    return this._player;
  }

  set player(player: TNSPlayer) {
    if (this._player) {
      this._player.dispose();
    }
    this._player = player;
  }

  clearAudio() {
    if (this.audio) {
      this.audio = null;
    }
  }
}
