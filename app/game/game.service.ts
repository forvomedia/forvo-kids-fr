import { Injectable } from '@angular/core';
import { switchMap } from 'rxjs/operators';
import { GameItem } from '../core/model/game-item';
import { CategoryService } from '../core/service/category.service';
import { CategoryDbService } from '../core/service/device/category.db.service';
import { WordDbService } from '../core/service/device/word.db.service';
import { GameItemService } from '../core/service/game-item.service';
import { PlayerService } from '../core/service/player.service';
import { chunkify, shuffleArray } from '../core/utils/collection.util';
import { CategoryGroupInterface } from './category-group.interface';
import { GameInterface } from './game.interface';

@Injectable()
export class GameService {
  currentGame: GameInterface;
  results: { [key: string]: number } = {};
  items: GameItem[];
  groupedItems: any[];
  currentCategoryGroups: CategoryGroupInterface;

  constructor(
    private categoryService: CategoryService,
    private gameItemService: GameItemService,
    private playerService: PlayerService,
    private categoryDbService: CategoryDbService,
    private wordDbService: WordDbService
  ) { }

  getCategories() {
    return this.categoryService.getCategories();
  }

  getCategoryById(categoryId) {
    return this.categoryService.getCategoryById(categoryId).toPromise();
  }

  getItemsByCategory(categoryId: number) {
    return this.categoryService.getCategoryById(categoryId)
      .pipe(switchMap((category) => this.gameItemService.getItemsByCategory(category))).toPromise();
  }

  getGroupsOfItemsByCategory(categoryId: number) {
    return this.getItemsByCategory(categoryId)
      .then((items) => this.splitItems(items))
      .catch(console.log.bind(console));
  }

  private splitItems(items: GameItem[]) {
    this.items = this.shuffleItems(items);

    if (this.items.length >= 10) {

      let chunks = 0;
      switch (this.items.length) {
        case 26:  // 6-5-5-5-5
          chunks = 5;
          break;
        case 24:  // 6-6-6-6
        case 20:  // 5-5-5-5
          chunks = 4;
          break;
        case 18:  // 6-6-6
        case 17:  // 6-6-5
        case 15:  // 5-5-5
          chunks = 3;
          break;
        case 11:  // 6-5
        case 10:  // 5-5
          chunks = 2;
          break;
      }

      this.groupedItems = chunkify(this.items, chunks, true);
    } else {
      this.groupedItems = Array(this.items);
    }

    return this.groupedItems;
  }

  private shuffleItems(items: GameItem[]) {
    this.items = shuffleArray(items);
    return this.items;
  }

  playOnLoop(audio: string) {
    return this.playerService.playOnLoop(audio);
  }

  play(audio: string, callback = null) {
    return this.playerService.play(audio, callback);
  }

  replay() {
    this.playerService.replay();
  }

  clearAudio() {
    this.playerService.clearAudio();
  }

  saveResult() {
    this.currentGame.score = this.currentGame.correct / this.currentGame.answers;
    this.results[this.currentGame.categoryId.toString()] = this.currentGame.score;
  }

  clearCurrentCategoryCroups() {
    this.currentCategoryGroups = null;
  }

  clearCategoryResults(categoryId: number) {
    this.results[categoryId.toString()] = null;
  }

  saveCategoryVisit(categoryId: number) {
    this.categoryDbService.insertCategoryVisit(categoryId);
  }

  saveTestResponse(categoryId: number, wordId: number, failure: number) {
    this.wordDbService.saveTestResponse(categoryId, wordId, failure);
  }
}
