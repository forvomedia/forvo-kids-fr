import { Component, NgZone, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { ERROR_AUDIO, SUCCESS_AUDIO } from '../../../core/config/constants';
import { GameItem } from '../../../core/model/game-item';
import { shuffleArray } from '../../../core/utils/collection.util';
import { GameService } from '../../game.service';

@Component({
  selector: 'app-game-test',
  moduleId: module.id,
  templateUrl: './game-test.component.html',
  styleUrls: ['./game-test.component.css']
})
export class GameTestComponent implements OnInit {
  processing = false;
  itemIndex = 0;
  items: GameItem[];
  categoryItems: GameItem[];
  categoryId: number;
  isFirstTestResponse = true;

  constructor(
    private gameService: GameService,
    private zone: NgZone,
    private router: Router,
    private activeRoute: ActivatedRoute
  ) { }

  async ngOnInit() {
   this.categoryId = +this.activeRoute.parent.parent.snapshot.params['id'];
    this.categoryItems = shuffleArray(await this.gameService.getItemsByCategory(this.categoryId));

    if (this.gameService.currentCategoryGroups) {
      this.items = shuffleArray(this.gameService.currentCategoryGroups.groups[this.gameService.currentCategoryGroups.currentGroup]);

      if (this.gameService.currentCategoryGroups.currentGroup === 0) {
        const category = await this.gameService.getCategoryById(this.categoryId);
        this.gameService.currentGame = { answers: 0, correct: 0, categoryId: this.categoryId, categoryName: category.name};
        this.gameService.currentCategoryGroups.started = 1;
      }
    }

    this.playAudio();
  }

  get item() {
    return (this.items) ? this.items[this.itemIndex] : null;
  }

  playAudio() {
    this.gameService.play(`${ this.item.audio }`);
  }

  onSuccess(success: boolean) {
    this.gameService.currentGame.answers++;
    if (success) {
      this.gameService.currentGame.correct++;
      this.processing = true;
      this.gameService.play(SUCCESS_AUDIO, this.goToNext);
    } else {
      this.gameService.play(ERROR_AUDIO);
      this.processing = false;
    }

    if (this.isFirstTestResponse) {
      const failure = success ? 0 : 1;
      this.gameService.saveTestResponse(this.categoryId, this.item.id, failure);
      this.isFirstTestResponse = false;
    }
  }

  private goToNext = () => {
    if (this.itemIndex < this.items.length - 1) {
      this.zone.run(() => {
        this.itemIndex++;
        this.playAudio();
        this.processing = false;
        this.isFirstTestResponse = true;
      });
    } else {
      if (this.gameService.currentCategoryGroups.currentGroup < (this.gameService.currentCategoryGroups.length - 1 )) {
        // NAVIGATE TO LEARN
        this.zone.run(() => this.router.navigate(['learn'], { relativeTo: this.activeRoute.parent.parent }));
      } else {
        this.gameService.saveResult();
        this.zone.run(() => this.router.navigate(['result'], { relativeTo: this.activeRoute.parent.parent }));
      }
    }
  }
}
