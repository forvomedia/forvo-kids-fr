import { Component, EventEmitter, Input, OnChanges, Output, SimpleChanges } from '@angular/core';
import { RouterExtensions } from 'nativescript-angular';
import { GameItem } from '../../../../core/model/game-item';
import { shuffleArray } from '../../../../core/utils/collection.util';
import { ICON_ERROR_IMAGE, ICON_SUCCESS_IMAGE } from '../../../../core/config/constants';
import { DeviceService } from '../../../../core/service/device.service';

@Component({
  selector: 'app-game-test-cards',
  moduleId: module.id,
  templateUrl: './game-test-cards.component.html',
  styleUrls: ['./game-test-cards.component.css']
})
export class GameTestCardsComponent implements OnChanges {
  testItems: GameItem[];
  @Input() categoryId: number;
  @Input() item: GameItem;
  @Input() all: GameItem[];
  @Input() blocked: boolean;
  @Output() success = new EventEmitter<boolean>();

  constructor(
    private deviceService: DeviceService,
    public routerExtensions: RouterExtensions
  ) { }

  ngOnChanges(changes: SimpleChanges) {
    if (changes.item || changes.all) {
      const wrongItems = shuffleArray(this.all.filter(item => item.id !== this.item.id)).slice(0, 3);
      this.testItems = shuffleArray([this.item, ...wrongItems]);
    }
  }

  onSelected(item: GameItem) {
    if (!this.blocked) {
      const success = (this.item.id === item.id);
      item.sendFeedback(success);
      this.success.emit(success);
    }
  }

  getColumn(index: any) {
   return  Math.floor(index / 2);
  }

  get iconErrorImage() {
    return `${ ICON_ERROR_IMAGE }`;
  }

  get iconSuccessImage() {
    return `${ ICON_SUCCESS_IMAGE }`;
  }

  deviceIsPhone() {
    return this.deviceService.deviceIsPhone();
  }

  deviceIsIOSPhoneFive() {
    return this.deviceService.deviceIsIOSPhoneFive();
  }
}
