import { Component, OnDestroy, OnInit, NgZone } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { TNSPlayer } from 'nativescript-audio';
import { START_AUDIO, TRANSLATION_GAME_START } from '../../../core/config/constants';
import { PlayerService } from '../../../core/service/player.service';
import { DeviceService } from '../../../core/service/device.service';
import { TranslationService } from '~/core/service/translation.service';

@Component({
  selector: 'app-game-start',
  moduleId: module.id,
  templateUrl: './game-start.component.html',
  styleUrls: ['./game-start.component.css']
})
export class GameStartComponent implements OnInit, OnDestroy {
  private player: TNSPlayer;
  translations: string[];
  translation_game_start: string;

  constructor(
    private playerService: PlayerService,
    private deviceService: DeviceService,
    private translationService: TranslationService,
    private router: Router,
    private activeRoute: ActivatedRoute,
    private zone: NgZone
  ) {
    const translationIds = [TRANSLATION_GAME_START];
    this.translation_game_start = TRANSLATION_GAME_START;
    this.translations = this.translationService.getSectionTranslations(translationIds);
  }

  ngOnInit() {
    this.player = this.playerService.play(
      START_AUDIO,
      () => this.zone.run(() => this.router.navigate(['test'], { relativeTo: this.activeRoute.parent }))
    );
  }

  ngOnDestroy() {
    if (this.player) {
      this.player.dispose();
    }
  }

  deviceIsPhone() {
    return this.deviceService.deviceIsPhone();
  }

  getTranslation(id: string) {
    return (this.translations) ? this.translations[id] : null;
  }
}
