import { Component } from '@angular/core';
import { Page } from 'tns-core-modules/ui/page';
import { DeviceService } from '../../core/service/device.service';

@Component({
  selector: 'app-game-category',
  moduleId: module.id,
  templateUrl: './game-category.component.html',
  styleUrls: ['./game-category.component.css']
})
export class GameCategoryComponent {
  constructor(
    page: Page,
    private deviceService: DeviceService
  ) {
    page.actionBarHidden = true;

    if (this.deviceService.deviceIsIOS()) {
      if (this.deviceService.deviceIsIPhoneX()) {
        page.className = 'iphonex';
      } else {
        page.className = 'iphone';
      }
    }
  }
}
