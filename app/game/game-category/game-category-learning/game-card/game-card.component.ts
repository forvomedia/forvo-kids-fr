import { Component, EventEmitter, Input, OnChanges, OnDestroy, Output } from '@angular/core';
import { RouterExtensions } from 'nativescript-angular';
import { TNSPlayer } from 'nativescript-audio';
import { GameItem } from '../../../../core/model/game-item';
import { GameService } from '../../../game.service';
import { DeviceService } from '../../../../core/service/device.service';

@Component({
  selector: 'app-game-card',
  moduleId: module.id,
  templateUrl: './game-card.component.html',
  styleUrls: ['./game-card.component.css']
})
export class GameCardComponent implements OnDestroy, OnChanges {
  @Input() item: GameItem;
  @Output() next = new EventEmitter<void>();
  private forwardingEnabled = false;
  private player: TNSPlayer;

  constructor(
    private gameService: GameService,
    private deviceService: DeviceService,
    public routerExtensions: RouterExtensions
  ) { }

  ngOnChanges() {
    this.forwardingEnabled = false;
    this.player = this.gameService.play(`${ this.item.audio }`, () => this.forwardingEnabled = true);
  }

  ngOnDestroy() {
    this.player.dispose();
  }

  goToNext() {
    if (this.forwardingEnabled) {
      this.next.emit();
    }
  }

  deviceIsPhone() {
    return this.deviceService.deviceIsPhone();
  }

  deviceIsIOSPhoneFive() {
    return this.deviceService.deviceIsIOSPhoneFive();
  }
}
