import { NgModule } from '@angular/core';
import { NativeScriptRouterModule } from 'nativescript-angular';
import { NativeScriptModule } from 'nativescript-angular/nativescript.module';

@NgModule({
  exports: [
    NativeScriptModule,
    NativeScriptRouterModule
  ]
})
export class SharedModule { }
